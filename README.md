# greenseer

![](https://scontent-lga3-2.xx.fbcdn.net/v/t1.18169-1/20622274_192292024640461_8881449260197346247_n.jpg?stp=c19.0.148.148a_dst-jpg_p148x148&_nc_cat=104&ccb=1-7&_nc_sid=1eb0c7&_nc_ohc=2Lpl-E-_o8UAX9Rpdh_&_nc_oc=AQm-VVifpCrhyHgAUpf9vuZzs-daux6c4Uqdsmk3mvPALjcT_xvoOYfTfWxDjJCS29U&_nc_ht=scontent-lga3-2.xx&oh=00_AT-HtAOPRH97sMntCXW0SkEjiIw2hkq0XyI-d8oAqc-jhg&oe=62E40259)  

**in development, not functional**

Minimal edge function that receives a GET request with a manifest URI argument and returns HTML with a IIIF image viewer

- basically, want to get a zoomable IIIF image with <iframe src="">. In the src attribute you'd have, for example: `<iframe src="https://www.greenseer.io/?URI=https://figgy.princeton.edu/concern/scanned_resources/d38b91c7-ee13-4b2f-afdb-ebe4dd95a5b9/manifest" />`

- TODO possible to know type of referring element? If <img>, then return default.jpg, else return OSD viewer. 

If it's a collection URI, then... 
